(define-package "magit-forge" "0.4.5"
  "Access Git forges from Magit."
  '((emacs          "29.1")
    (compat         "30.0.0.0")
    (closql         "2.1.0")
    (dash           "2.19.1")
    (emacsql-sqlite "4.1.0")
    (ghub           "4.2.0")
    (let-alist      "1.0.6")
    (magit          "4.1.3")
    (markdown-mode  "2.6")
    (seq            "2.24")
    (transient      "0.8.0")
    (yaml           "0.5.5"))
  :homepage "https://github.com/magit/forge"
  :keywords '("git" "tools" "vc"))
